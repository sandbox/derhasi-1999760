<?php

/**
 * Rules actions for math implementation.
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_math_rules_action_info() {
  // Add actions that depend on required modules only.
  $actions = array(
    'math_expr' => array(
      'label' => t('Math expression'),
      'parameter' => array(
        'expression' => array(
          'type' => 'text',
          'label' => t('Expression'),
          'description' => t('Enter mathematical expressions such as 2 + 2 or sqrt(5). You may assign variables and create mathematical functions and evaluate them. Use the ; to separate these. For example: f(x) = x + 2; f(2).'),
          'restriction' => 'input',
        ),
        'precision' => array(
          'type' => 'integer',
          'label' => t('Precision'),
          'description' => t('Rounds to calulcated value to the given precision'),
          'default value' => '-1',
          'options list' => 'rules_math_precision_list',
        ),
      ),
      'group' => t('Data'),
      'base' => 'rules_math_action_math_expression',
      'provides' => array(
        'result' => array(
          'type' => 'decimal',
          'label' => t('Calculation result'),
        ),
      ),
    ),
  );
  return $actions;
}

/**
 * Provides rules action callback to execute ctools math expressions.
 *
 * @param string $expression_input
 *   Math expression
 * @param integer $precision
 *   Integer of decimal precision (-1 means, no decimal rounding).
 *
 * @return array
 *   - result: decimal of the calculated value
 */
function rules_math_action_math_expression($expression_input, $precision) {
  ctools_include('math-expr');
  $expressions = explode(';', $expression_input);
  $math = new ctools_math_expr;
  foreach ($expressions as $expression) {
    if ($expression !== '') {
      $value = $math->evaluate($expression);
    }
  }

  // Round value to the given precision.
  if ($precision >= 0) {
    $value = round($value, $precision);
  }

  return array(
    'result' => $value,
  );
}

/**
 * Helper for retrieving precision array.
 *
 * @return array
 */
function rules_math_precision_list() {
  return array(
    -1 => t('none'),
    0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8
  );
}
